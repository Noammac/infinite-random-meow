#!/bin/bash
echo 'To exit the program, press the "q" button'

list=`espeak --voices | tail -n +2 | awk '{print $2}'`

while true
	do
		amp=`shuf -i 25-200 -n 1`
		pitch=`shuf -i 0-99 -n 1`
		speed=`shuf -i 0-450 -n 1`
		meow=`yes "meow " | head -n $(shuf -i 1-18 -n 1)`
		voice=`echo "$list" | shuf -n 1`
		espeak -a $amp -p $pitch -s $speed -v $voice "$meow"
done &

while_pid=$!

while true
    do
        read -n 1 -s
        case $REPLY in
            q|Q)
                break
                ;;
            *)
                continue
                ;;
        esac
done

kill $while_pid
